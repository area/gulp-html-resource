var fileinclude=require("gulp-file-include"),
	template=require("gulp-template"),
	gulp=require("gulp"),
	gulpSequence = require("gulp-sequence"),
	config=require("../resConfig");

var _config=new config();

var htmlResource=function(options){
	this.version="1.0";
	this.options=options;
	this.config=_config;
	this.init();
};

htmlResource.prototype={
	init:function(){
		this._includeHtml();
	},
	_includeHtml:function(){
		console.log("start...");
		console.log(JSON.stringify(this.config));
		return gulp.src(this.config['template'])
			.pipe(fileinclude({
			  prefix: '#',
			  basepath: './'
			}))
			.pipe(template({
				cssPath:this.config['css'],
				jsPath:this.config['js'],
				imgPath:this.config['image'],
				suffix:this.config['suffix']
			}))
			.pipe(gulp.dest(this.config['output']));
	}
};

module.exports=htmlResource;