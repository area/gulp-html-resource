var development="pro";

module.exports=function(){
	if(development=="dev"){
		return{
			suffix:"",
			output:"html",
			template:"template/**/*.html",
			css:"dev/stylesheets",
			js:"dev/javascripts",
			image:"dev/images"
		}
	}
	if(development=="pro"){
		return{
			suffix:".min",
			output:"html",
			template:"template/**/*.html",
			css:"build/stylesheets",
			js:"build/javascripts",
			image:"build/images"
		}
	}
}