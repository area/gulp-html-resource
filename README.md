#gulp-html-resource

前端UI开发中，我们经常遇到开发引用资源路径和打包后的路径不在同一个目录，并且css，js都会进行压缩构建。
在页面测试过程，会进常手动更改路径（既麻烦又容易出错~）。

现在 就算是静态html，我们同样可以使用模板化来构建我们的UI页面啦~~

## 解决的痛点
1. html模板引用资源的可配置化
2. 可以同时进行开发调试和生产调试
3. 解放频繁修改路径的刚度做法~~
4. 不用依赖其他的服务端模板（ejs，jade)等

## 使用方式
需要一个resConfig.js配置文件
```
//调试模式（dev:开发模式，pro:生产模式）
var development="pro";

module.exports=function(){
	if(development=="dev"){
		return{
			suffix:"",//css,js文件后缀
			output:"html",//模板输出目录
			template:"template/**/*.html",//模板目录
			css:"dev/stylesheets",//开发版css目录
			js:"dev/javascripts",//开发版js目录
			image:"dev/images"//开发版图片目录
		}
	}
	if(development=="pro"){
		return{
			suffix:".min",
			output:"html",
			template:"template/**/*.html",
			css:"build/stylesheets",
			js:"build/javascripts",
			image:"build/images"
		}
	}
}
```

## Template
配置需要的模板：
例如：meta.html
```
<link rel="stylesheet" href="../<%=cssPath%>/main<%=suffix%>.css" >
<script src="../<%=jsPath%>/main<%=suffix%>.js"></script>
```
index.html
```
<!DOCTYPE html>
<html>
  <head>
    #include('./template/common/meta.html')
    <link rel="stylesheet" href="../<%=cssPath%>/test<%=suffix%>.css" >
    <script src="../<%=jsPath%>/test<%=suffix%>.js"></script>
  </head>
  <body>
    <img src="../<%=imgPath%>/1.png" />
  </body>
</html>
```

## Html
输出后的html：

开发模式：
```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="../dev/stylesheets/main.css" >
	<script src="../dev/javascripts/main.js"></script>
    <link rel="stylesheet" href="../dev/stylesheets/test.css" >
	<script src="../dev/javascripts/test.js"></script>
  </head>
  <body>
    <img src="../dev/images/1.png" />
  </body>
</html>
```

生产模式：
```
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="../build/stylesheets/main.min.css" >
    <script src="../build/javascripts/main.min.js"></script>
    <link rel="stylesheet" href="../build/stylesheets/test.min.css" >
	<script src="../build/javascripts/test.min.js"></script>
  </head>
  <body>
    <img src="../build/images/1.png" />
  </body>
</html>
```

## 初始化
在gulpfile.js中配置：
```
var gulp=require("gulp"),
	templateToHtml=require("./lib/index");


gulp.task("templateToHtml",function(){
	var resource=new templateToHtml();
});

```

